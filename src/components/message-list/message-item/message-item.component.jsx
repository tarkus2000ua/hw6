import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import './message-item.styles.css';

const MessageItem = ({
  message,
  handleLike,
  deleteMessage,
  editMessage,
  currentUser
}) => (
  <div
    className={`message-item ${message.userId === currentUser.id ? 'current' : ''}`}
  >
    <div className="avatar-wrapper">
      <img className="avatar" src={message.avatar} alt="avatar" />
    </div>
    <div className="message-wrapper">
      <div className="message-header">
        <div className="message-author">{message.user}</div>
        <div className="message-date">
          {moment(message.createdAt).format('hh:mm Do MMMM')}
        </div>
      </div>
      <div className="message-text">{message.text}</div>
      <div className="message-status">
        <div className="like" onClick={() => handleLike(message.id)}>
          <i className="far fa-heart"></i>
          <span className="like-counter">{message.likesCount}</span>
        </div>
        <div className="edit" onClick={() => editMessage(message.id)}>
          <i className="fas fa-pencil-alt"></i>
          <span className="like-counter">Edit</span>
        </div>
        <div
          className="delete"
          onClick={() => deleteMessage(message.id)}
        >
          <i className="far fa-trash-alt"></i>
          <span className="like-counter">Delete</span>
        </div>
      </div>
    </div>
  </div>
);

MessageItem.propTypes = {
  message: PropTypes.shape({
    id: PropTypes.string.isRequired,
    likesCount: PropTypes.number.isRequired,
    userId: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    createdAt: PropTypes.string,
    editedAt: PropTypes.string,
  }).isRequired,
  currentUser:PropTypes.shape({
    id:PropTypes.string.isRequired,
    name:PropTypes.string.isRequired
  })
}

export default MessageItem;
