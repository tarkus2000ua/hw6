import React, { Component } from 'react';

import './message-edit.styles.css';

class MessageEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: this.props.message.text
    };
  }

  onChange = (e) => {
    this.setState({ text: e.target.value });
  };

  onSubmit = (e) => {
    const { message } = this.props;
    e.preventDefault();
    this.props.update(message.id, this.state.text);
    this.setState({ text: '' });
  };

  render() {
    const { cancel } = this.props;
    return (
      <form className="message-edit" onSubmit={(e) => this.onSubmit(e)}>
        <div className="edit-wrapper">
          <textarea
            className="text"
            value={this.state.text}
            onChange={(e) => this.onChange(e)}
          ></textarea>
          <button type="submit" className="btn btn-save">
            Save
          </button>
          <button className="btn btn-cancel" onClick={cancel}>
            Cancel
          </button>
        </div>
      </form>
    );
  }
}

export default MessageEdit;
