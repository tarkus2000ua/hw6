import React, { Component } from 'react';

import './message-input.component.css';

class MessageInput extends Component {
  constructor() {
    super();
    this.state = {
      text: ''
    };
  }

  onChange = (e) => {
    this.setState({ text: e.target.value });
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.addMessage(this.state.text);
    this.setState({ text: '' });
  };

  render() {
    return (
      <div className="container">
        <form className="message-input" onSubmit={(e) => this.onSubmit(e)}>
          <textarea
            className="text"
            placeholder="Message"
            onChange={(e) => this.onChange(e)}
            value={this.state.text}
          ></textarea>
          <button className="btn">Send</button>
        </form>
      </div>
    );
  }
}

export default MessageInput;
