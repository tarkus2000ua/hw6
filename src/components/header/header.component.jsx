import React from 'react';
import PropTypes from 'prop-types';

import './header.styles.css';

const Header = ({ info }) => {
  return (
    <header className="header">
      <div className="container">
        <div className="chat-name">{info.chatName}</div>
        <div className="users-count">{info.usersCount} participants</div>
        <div className="messages-count">{info.messagesCount} messages</div>
        <div className="last-date">last message at: {info.lastDate}</div>
      </div>
    </header>
  );
};

Header.propTypes = {
  info: PropTypes.shape({
      chatName: PropTypes.string.isRequired,
      usersCount: PropTypes.number.isRequired,
      messagesCount: PropTypes.number.isRequired,
      lastDate: PropTypes.string.isRequired,
  }).isRequired,
}

export default Header;
