import React, { Component } from 'react';
import Header from './components/header/header.component.jsx';
import MessageList from './components/message-list/message-list.component.jsx';
import MessageInput from './components/message-input/message-input.component.jsx';
import MessageEdit from './components/message-list/message-item/message-edit/message-edit.component';
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';

class Chat extends Component {
  constructor() {
    super();

    this.state = {
      chatName: 'MyChat',
      isLoading: true,
      messageToEdit: null,
      scrollDown: true,
      messages: [],
      likes: [],
      currentUser: {
        id: 'myId',
        name: 'Andrii'
      }
    };
  }

  componentDidMount() {
    fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
      .then((response) => response.json())
      .then((response) =>
        response.map((message) => {
          return {
            // messageId: uuidv4(),
            likesCount: 0,
            ...message
          };
        })
      )
      .then((messageList) =>
        this.setState({
          messages: messageList,
          isLoading: false
        })
      )
      .catch((err) => console.log(err));

    this.disableScrollDownOnStateChange();
  }

  handleLike = (id) => {
    const { likes, currentUser, messages } = this.state;
    const index = likes.findIndex(
      (like) => like.id === id && like.userId === currentUser.Id
    );
    const messageList = [...messages];
    if (index !== -1) {
      likes.splice(index, 1);
      messageList.find((message) => message.id === id).likesCount -= 1;
    } else {
      likes.push({
        id: id,
        userId: currentUser.Id
      });
      messageList.find((message) => message.id === id).likesCount += 1;
    }
    this.setState({
      messages: messageList
    });
  };

  addMessage = (text) => {
    const { currentUser, messages } = this.state;
    const message = {
      id: uuidv4(),
      userId:currentUser.id,
      user: currentUser.name,
      text: text,
      avatar: `https://robohash.org/${currentUser.id}?set=set2`,
      editedAt: '',
      createdAt: moment(Date.now()).format(),
      likesCount: 0
    };
    this.setState({
      messages: [...messages, message],
      scrollDown: true
    });

    this.disableScrollDownOnStateChange();
  };

  deleteMessage = (id) => {
    const { messages } = this.state;
    const messageList = [...messages];

    const index = messageList.findIndex((message) => message.id === id);
    if (index !== -1) {
      messageList.splice(index, 1);
      this.setState({
        messages: messageList
      });
    }
  };

  editMessage = (id) => {
    const { messages } = this.state;
    const messageToEdit = messages.find((message) => message.id === id);
    this.setState({
      messageToEdit: messageToEdit
    });
  };

  cancelEdit = () => {
    this.setState({
      messageToEdit: null
    });
  };

  updateMessage = (id, text) => {
    const { messages } = this.state;
    const messageList = [...messages];

    const messageToUpdate = messageList.find(
      (message) => message.id === id
    );
    if (messageToUpdate) {
      messageToUpdate.text = text;
      this.setState({
        messages: messageList,
        messageToEdit: null
      });
    }
  };

  disableScrollDownOnStateChange = () => {
    setTimeout(() => {
      this.setState({
        scrollDown: false
      });
    }, 1000);
  };

  render() {
    const {
      messages,
      chatName,
      currentUser,
      isLoading,
      messageToEdit,
      scrollDown
    } = this.state;
    const usersCount = new Set(messages.map((message) => message.user)).size;
    const messagesCount = messages.length;
    let lastDate = [...messages]
      .map((message) => new Date(message.createdAt).getTime())
      .sort((a, b) => b - a)[0];
    lastDate = moment(lastDate).format('hh:mm DD.MM.YY');
    const info = {
      chatName,
      usersCount,
      messagesCount,
      lastDate
    };
    return (
      <div className="chat">
        <Header info={info} />
        <MessageList
          messages={messages}
          handleLike={this.handleLike}
          deleteMessage={this.deleteMessage}
          editMessage={this.editMessage}
          currentUser={currentUser}
          isLoading={isLoading}
          scrollDown={scrollDown}
        />
        {!isLoading && <MessageInput addMessage={this.addMessage} />}
        {messageToEdit && (
          <MessageEdit
            message={messageToEdit}
            cancel={this.cancelEdit}
            update={this.updateMessage}
          />
        )}
      </div>
    );
  }
}

export default Chat;
